import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import axios from "axios";
import { ImArrowUp } from "react-icons/im";
import LocationData from "../assets/city.list.json";

class CityDetails extends Component {
	static defaultProps = {
		API_BASE_URL: "https://api.openweathermap.org/data/2.5/onecall",
		API_KEY: "69843a18870037f351c1cf6547e50cdc",
	};
	constructor(props) {
		super(props);
		this.state = {
			city: "",
			forecast: [],
		};
	}

	async componentDidMount() {
		const { id } = this.props.match.params;
		const currentLocation = LocationData.filter((location) => location.id === parseInt(id))[0];
		let { lat, lon } = currentLocation.coord;
		const { API_BASE_URL, API_KEY } = this.props;
		try {
			let res = await axios.get(API_BASE_URL, {
				params: {
					lat: lat,
					lon: lon,
					exclude: "minutely,hourly",
					appid: API_KEY,
				},
			});
			// console.log(res);
			this.setState({
				city: currentLocation.name,
				forecast: res.data,
			});
		} catch (error) {
			console.log(error);
		}
	}

	render() {
		const { city } = this.state;
		const currentData = { ...this.state.forecast.current };
		const currentDate = new Date(currentData.dt * 1000).toLocaleString();
		const currentWindDegree = currentData.wind_deg;
		const currentWindSpeed = currentData.wind_speed;
		const forecastData = { ...this.state.forecast.daily };
		// console.log(forecastData);
		const forecastWind = Object.entries(forecastData).map((day) => (
			<div
				className="d-flex flex-column justify-content-between border border-primary"
				key={day[1].dt}
			>
				<div className="justify-content-between border border-primary align-items-center p-2">
					<h6>{new Date(day[1].dt * 1000).toLocaleString()}</h6>
				</div>
				<div className="justify-content-center border border-primary align-items-center p-2">
					<ImArrowUp
						size={50}
						color={"blue"}
						style={{
							display: "block",
							marginLeft: "auto",
							marginRight: "auto",
							height: "60px",
							transform: `rotate(${day[1].wind_deg}deg)`,
						}}
					/>
				</div>
				<div className="d-flex flex-row justify-content-between mx-auto p-2">
					<div>
						<h6>{day[1].wind_speed} metre/sec</h6>
					</div>
				</div>
			</div>
		));
		return (
			<div>
				<div className="text-center  bg-primary text-white p-2">
					<h1>Wind Forecast for {city}</h1>
				</div>
				<div className="d-flex flex-row p-2 justify-content-between align-items-center">
					<div>
						<img
							src={require("../assets/compass.jpg").default}
							alt="Cardinal Direction"
							style={{ width: "400px", height: "400px" }}
						/>
					</div>
					<div className="d-flex flex-column p-2 justify-content-between align-items-center">
						<h2>Current wind</h2>
						<div className="d-flex flex-column justify-content-between">
							<h3>{currentDate}</h3>
							<div className="d-flex flex-row  justify-content-between m-2 p-5">
								<ImArrowUp
									size={50}
									color={"blue"}
									style={{
										display: "block",
										marginLeft: "auto",
										marginRight: "auto",
										height: "60px",
										transform: [`rotate(${currentWindDegree}deg) scale(2)`],
									}}
								/>
							</div>
							<div className="d-flex flex-row justify-content-between mx-auto p-2">
								<div>
									<h4>{currentWindSpeed} metre/sec</h4>
								</div>
							</div>
						</div>
					</div>
					<div>
						<img
							src={require("../assets/compass.jpg").default}
							alt="Cardinal Direction"
							style={{ width: "400px", height: "400px" }}
						/>
					</div>
				</div>
				<hr />
				<div className="d-flex flex-column p-2 justify-content-between align-items-center">
					<h2>Forecasted wind</h2>
					<div className="d-flex flex-row p-2 justify-content-between align-items-center">
						{forecastWind}
					</div>
				</div>
				<div className="d-flex flex-column p-2 justify-content-between align-items-center">
					<Link to="/locations" className="btn btn-info">
						Go Back
					</Link>
				</div>
			</div>
		);
	}
}

export default withRouter(CityDetails);
