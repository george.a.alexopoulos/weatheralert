import React, { Component } from "react";
import { MdSearch } from "react-icons/md";
import { MdClose } from "react-icons/md";
import "./SearchCity.css";

class SearchCity extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isSearching: false,
			searchTerm: "",
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleClearInput = this.handleClearInput.bind(this);
	}

	handleChange(e) {
		this.setState({
			isSearching: true,
		});
		this.setState({
			searchTerm: e.target.value,
		});
	}

	handleClearInput(e) {
		this.setState({
			isSearching: false,
		});
		this.setState({
			searchTerm: "",
		});
	}

	handleSelect(id) {
		this.props.addFavLocation(id);
		this.handleClearInput();
	}

	render() {
		const { LocationData } = this.props;
		return (
			<div className="Search">
				{/* <h1 className="display-3">Weather Alert App</h1> */}
				<div className="Search-SearchInputs rounded-pill border border-3">
					<input
						type="text"
						placeholder="Search for a city..."
						onChange={this.handleChange}
						value={this.state.searchTerm}
					/>
					<div className="Search-searchIcon">
						{this.state.searchTerm.length === 0 ? (
							<MdSearch />
						) : (
							<MdClose id="clearBtn" onClick={this.handleClearInput} />
						)}
					</div>
				</div>
				{this.state.isSearching && this.state.searchTerm.length !== 0 && (
					<div className="Search-dataresult">
						{LocationData.filter((location) => {
							return location.name
								.toLowerCase()
								.startsWith(this.state.searchTerm.toLowerCase());
						})
							.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1))
							.map((location) => {
								return (
									<div className="SearchInputs-container" key={location.id}>
										<div className="SearchInputs-list">
											<div
												className="SearchInputs-city"
												onClick={this.handleSelect.bind(this, location.id)}
											>
												<div className="SearchInputs-city-name">
													{location.name}
												</div>
												<div className="SearchInputs-city-country">
													{location.country}
												</div>
											</div>
										</div>
									</div>
								);
							})}
					</div>
				)}
			</div>
		);
	}
}

export default SearchCity;
