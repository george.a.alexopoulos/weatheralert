import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import SearchCity from "./SearchCity";
import LocationData from "../assets/city.list.json";
import "./CityList.css";

class CityList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			favCities: JSON.parse(window.localStorage.getItem("favoriteCities") || "[]"),
		};
		this.addFavLocation = this.addFavLocation.bind(this);
	}

	favCityExists(list, prop, val) {
		if (list.length > 0) {
			for (let i in list) {
				if (list[i][prop] === val) {
					return true;
				}
			}
		} else {
			return false;
		}
	}

	addFavLocation(locationId) {
		let location = LocationData.find((l) => l.id === locationId);
		console.log(location);

		let selectedCity = {
			id: locationId,
			name: location.name,
			state: location.state,
			country: location.country,
			coord: {
				lon: location.coord.lon,
				lat: location.coord.lat,
			},
		};

		// console.log("selectedCity: ", JSON.stringify(selectedCity));
		// console.log("selectedCity: ", selectedCity);
		let favoriteCities = JSON.parse(window.localStorage.getItem("favoriteCities")) || [];
		// console.log("favoriteCities: ", favoriteCities);
		const isIncluded = this.favCityExists(favoriteCities, "id", selectedCity.id);
		// console.log(isIncluded);
		if (favoriteCities === null) favoriteCities = [];
		if (!isIncluded) {
			favoriteCities.push(selectedCity);
			this.setState({ favCities: favoriteCities }, () =>
				window.localStorage.setItem("favoriteCities", JSON.stringify(favoriteCities))
			);
		}
	}

	handleClick(id) {
		const { history } = this.props;
		history.push(`/locations/${id}`);
	}

	render() {
		return (
			<div>
				<div className="text-center  bg-primary text-white p-2">
					<h1>Wind Forecast API</h1>
				</div>
				<SearchCity LocationData={LocationData} addFavLocation={this.addFavLocation} />
				<div className="CityList">
					<h2>Favorite Cities</h2>
					<h6>(select for details...)</h6>
					<div className="CityList-list">
						{this.state.favCities
							.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1))
							.map((city) => {
								return (
									<div
										className="CityList-city rounded-pill"
										key={city.id}
										onClick={this.handleClick.bind(this, city.id)}
									>
										<div className="CityList-city-name">{city.name}</div>
										<div className="CityList-city-country">{city.country}</div>
									</div>
								);
							})}
					</div>
				</div>
			</div>
		);
	}
}

export default withRouter(CityList);
