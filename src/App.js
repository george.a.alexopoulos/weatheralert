import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import CityList from "./components/CityList";
import CityDetails from "./components/CityDetails";
import "./App.css";

class App extends Component {
	render() {
		return (
			<Switch>
				<Route exact path="/locations" render={() => <CityList />} />
				<Route exact path="/locations/:id" render={() => <CityDetails />} />
				<Redirect to="/locations" />
			</Switch>
		);
	}
}

export default App;
